package com.example.project1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.example.loginpage.UsersList
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import kotlin.Result.Companion.success

class MainActivity : BaseActivity(),LoginViewI {

    val builder = GsonBuilder()
    val gson = builder.serializeNulls().create()
    private val loginPresenter = LoginPresenter(this, this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        progressBarbaseactivity.visibility = View.GONE

        signinLogintextView.setOnClickListener {
            var intent = Intent(this, RegisterationActivity::class.java)
            startActivity(intent)
        }


        LoginbtnLoginbutton.setOnClickListener {
            val email = EmailLogineditText.text.toString().trim()
            val password = PasswordLogineditText.text.toString().trim()
            loginPresenter.callLoginApi(email, password, "1")

        }
    }

    override fun onBackPressed() {
        val intent = Intent(Intent.ACTION_MAIN)
        intent.addCategory(Intent.CATEGORY_HOME)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)

    }

    override fun onSuccess(loginBase: UsersList) {
        showMessage(loginBase.message!!)
    }

    override fun onError(error: Error) {
        showMessage(error.message!!)
    }
}
