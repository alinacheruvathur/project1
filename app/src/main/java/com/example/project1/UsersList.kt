package com.example.loginpage

import com.example.project1.Users
import com.google.gson.annotations.SerializedName
import java.util.*

data class UsersList (val status: String?=null,
                      val message: String?=null,
                      val date: Users?=null)

data class Error(val status: String?,
                 val message: String?)