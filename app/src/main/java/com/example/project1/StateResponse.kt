package com.example.project1

data class StateResponse (

    val data : List<StateData>,
    val message : String,
    val status : String

)