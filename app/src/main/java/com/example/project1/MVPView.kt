package com.example.project1

interface MVPView {
    fun showLoading()
    fun hideLoading()
    fun netWorkConnected():Boolean
    fun showMessage(message:String)
}