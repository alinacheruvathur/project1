package com.example.project1

import com.example.loginpage.UsersList

interface LoginViewI:MVPView {

    fun onSuccess(loginBase: UsersList)
    fun onError(error: Error)

}