package com.example.project1

data class CityResponse(

    val data: List<CityData>,
    val message: String,
    val status: String

)
