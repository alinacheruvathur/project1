package com.example.project1

import android.content.Context
import com.example.loginpage.UsersList
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginPresenter(var LoginViewi: LoginViewI,var context: Context):LoginPresenterI {

    val builder = GsonBuilder()
    val gson = builder.serializeNulls().create()

    override fun callLoginApi(emailId: String, password: String, providerType: String) {

        LoginViewi.showLoading();
        if(LoginViewi.netWorkConnected()){

            RetrofitClient.instance.login(emailId,password,1).
            enqueue(object: Callback<JsonObject> {
                override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                    when {
                        response.code() == 400 -> {
                            val loginBase = gson.fromJson(response.errorBody()?.charStream(), Error::class.java)
                            LoginViewi.onError(loginBase)
                        }
                        response.code() == 200 -> {
                            val loginBase = gson.fromJson(response.body().toString(), UsersList::class.java)
                            LoginViewi.onSuccess(loginBase)
                        }
                        else -> {
                            LoginViewi.showMessage(context.resources.getString(R.string.something_went))
                        }


                    }
                }

                override fun onFailure(call: Call<JsonObject>, t: Throwable) {

                }

            })


        }else{
            LoginViewi.hideLoading()
            LoginViewi.showMessage(context.resources.getString(R.string.no_internet))
        }
    }
}